package com.andrescardona.examples;

import com.andrescardona.examples.AppConfigs;
import com.andrescardona.examples.Notifications;
import com.andrescardona.examples.serde.AppSerdes;
import com.andrescardona.examples.types.Notification;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

import static org.apache.kafka.streams.kstream.Printed.toSysOut;

public class RewardsAppAggregate {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        streamsBuilder.stream(AppConfigs.posTopicName,
                        Consumed.with(AppSerdes.String(), AppSerdes.PosInvoice()))
                .filter((key, value) -> value.getCustomerType().equalsIgnoreCase(AppConfigs.CUSTOMER_TYPE_PRIME))
                .groupBy((key, value) -> value.getCustomerCardNo(), Grouped.with(AppSerdes.String(), AppSerdes.PosInvoice()))
                .aggregate(
                        //initializer
                        () -> new Notification()
                                .withCustomerCardNo(null)
                                .withEarnedLoyaltyPoints(0D)
                                .withInvoiceNumber(null)
                                .withTotalAmount(0D)
                                .withTotalLoyaltyPoints(0D),
                        // Aggregator
                        (key, value, aggregator) ->
                                new Notification()
                                        .withCustomerCardNo(value.getCustomerCardNo())
                                        .withTotalAmount(value.getTotalAmount())
                                        .withTotalLoyaltyPoints(value.getTotalAmount() + aggregator.getTotalLoyaltyPoints()),
                        //Serializer )
                        Materialized.<String, Notification, KeyValueStore<Bytes, byte[]>>as(AppConfigs.REWARDS_STORE_NAME)
                        .withKeySerde(AppSerdes.String())
                        .withValueSerde(AppSerdes.Notification())
                ).toStream()
                .to(AppConfigs.notificationTopic, Produced.with(AppSerdes.String(), AppSerdes.Notification()));

        logger.info("Starting Stream");
        KafkaStreams stream = new KafkaStreams(streamsBuilder.build(), props);
        stream.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Stopping Streams");
            stream.close();
        }));
    }
}
