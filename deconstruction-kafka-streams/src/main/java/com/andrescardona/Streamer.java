package com.andrescardona;

import com.andrescardona.configuration.KafkaStreamsConfig;
import com.andrescardona.serde.AppSerdes;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Printed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Streamer {

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(Streamer.class);

        Properties props = new Properties();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaStreamsConfig.bootstrapServers);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, KafkaStreamsConfig.AppId);
        props.put(StreamsConfig.STATE_DIR_CONFIG, KafkaStreamsConfig.stateStoreLocation);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);
//        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, AppSerdes.String().getClass());
//        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,AppSerdes.Student().getClass());

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        streamsBuilder.stream(KafkaStreamsConfig.topic,
                Consumed.with(AppSerdes.String(), AppSerdes.Student()))
                .groupBy((key,value) -> value.getFaculty(), Grouped.with(AppSerdes.String(), AppSerdes.Student()))
                .count()
                .toStream().print(Printed.<String,Long>toSysOut().withLabel("Faculty :"));




        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), props);
        kafkaStreams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Closing down gracefully");
            kafkaStreams.close();
        }));

    }
}
