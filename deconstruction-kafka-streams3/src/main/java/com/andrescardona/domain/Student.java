
package com.andrescardona.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "lastName",
    "numberOfCourses",
    "faculty"
})
public class Student {

    @JsonProperty("name")
    private String name;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("numberOfCourses")
    private Double numberOfCourses;
    @JsonProperty("faculty")
    private String faculty;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Student withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Student withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("numberOfCourses")
    public Double getNumberOfCourses() {
        return numberOfCourses;
    }

    @JsonProperty("numberOfCourses")
    public void setNumberOfCourses(Double numberOfCourses) {
        this.numberOfCourses = numberOfCourses;
    }

    public Student withNumberOfCourses(Double numberOfCourses) {
        this.numberOfCourses = numberOfCourses;
        return this;
    }

    @JsonProperty("faculty")
    public String getFaculty() {
        return faculty;
    }

    @JsonProperty("faculty")
    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public Student withFaculty(String faculty) {
        this.faculty = faculty;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("lastName", lastName).append("numberOfCourses", numberOfCourses).append("faculty", faculty).toString();
    }

}
