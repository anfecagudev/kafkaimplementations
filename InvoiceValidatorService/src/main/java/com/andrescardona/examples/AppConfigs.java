package com.andrescardona.examples;

public class AppConfigs {
    public final static String applicationID = "PosValidator";
    public final static String bootstrapServers = "localhost:9092,localhost:9093,localhost:9094";
    public final static String groupID = "PosValidatorGroup";
    public final static String[] sourceTopicNames = {"invoices"};
    public final static String validTopicName = "validinvoices";
    public final static String invalidTopicName = "invalidinvoices";
}
