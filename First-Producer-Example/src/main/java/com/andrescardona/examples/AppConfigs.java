package com.andrescardona.examples;

class AppConfigs {
    final static String applicationID = "HelloProducer";
    final static String bootstrapServers = "localhost:9092, localhost:9093, localhost:9094";
    final static String topicName = "multitest";
    final static int numEvents = 30;
}
