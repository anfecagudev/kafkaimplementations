package com.andrescardona.examples;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import static com.andrescardona.examples.AppConfigs.eventFiles;
import static com.andrescardona.examples.AppConfigs.topicName;

public class DispatcherDemoWithCompletableParallelStreams {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        Properties props = new Properties();
        try (InputStream inputStream = new FileInputStream(AppConfigs.kafkaConfigFileLocation)) {
            props.load(inputStream);
            props.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfigs.applicationID);
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

            KafkaProducer<Integer, String> producer = new KafkaProducer<>(props);
            Arrays.stream(eventFiles)
                    .parallel()
                    .forEach(eventFile -> {
                            DispatcherForModernJava dispatcher = new DispatcherForModernJava(producer, topicName, eventFile);
                            dispatcher.sendMessage();
                        });
            producer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
