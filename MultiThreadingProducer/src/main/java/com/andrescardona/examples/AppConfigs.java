package com.andrescardona.examples;

class AppConfigs {


//    final static String bootstrapServers = "localhost:9092, localhost:9093, localhost:9094";
    final static String topicName = "multitest";

    final static String applicationID = "Multi-Threaded-Producer";

    final static String kafkaConfigFileLocation = "kafka.properties";
    final static String[] eventFiles = {"data/NSE05NOV2018BHAV.csv","data/NSE06NOV2018BHAV.csv"};
}
