package guru.learningjournal.kafka.examples;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Objects;
import java.util.Properties;

/**
 * Class created to singleton KafkaProducer
 * In @SpringBoot just create a bean
 * @param <K> Key Param for ProducerRecords and KafkaProducer.
 * @param <V> Value Param for ProducerRecords and KafkaProducer.
 */
public class KProducer<K,V>{

    private static KafkaProducer kafkaProducer;

    private static KProducer INSTANCE;

    private KProducer(){}

    public static KProducer getKProducer(){
        if (Objects.isNull(INSTANCE)){
            INSTANCE = new KProducer();
            kafkaProducer = getKProducer().getKafkaProducer();
        }
        return INSTANCE;
    }

    private KafkaProducer<K, V> getKafkaProducer(){
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfigs.applicationID);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, AppConfigs.transaction_id);
        return new KafkaProducer<K, V>(props);
    }

    public void produce(String topic, K key, V value){
        ProducerRecord<K,V> record = new ProducerRecord(topic, key, value);
        kafkaProducer.send(record);
    }

    public void beginTransaction(){
        kafkaProducer.beginTransaction();
    }

    public void initTransactions(){
        kafkaProducer.initTransactions();
    }

    public void abortTransaction(){
        kafkaProducer.abortTransaction();
    }

    public void close(){
        kafkaProducer.close();
    }

    public void commitTransaction(){
        kafkaProducer.commitTransaction();
    }


}
