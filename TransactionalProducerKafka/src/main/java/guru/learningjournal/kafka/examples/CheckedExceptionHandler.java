package guru.learningjournal.kafka.examples;


@FunctionalInterface
public interface CheckedExceptionHandler<GenType, ExObj extends Exception>{
    public void accept(GenType genType) throws ExObj;
}
