package guru.learningjournal.kafka.examples;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.function.Consumer;
import java.util.stream.Stream;



public class HelloProducerWithStreams {
    private static final Logger logger = LogManager.getLogger();
    static KProducer<Integer, String> kProducer =  KProducer.getKProducer();

    public static void main(String[] args) {
        logger.info("Starting First Transaction...");
        kProducer.initTransactions();
        kProducer.beginTransaction();
        Stream.of(1,2)
                .forEach(checkedExceptionHandler(number -> {
                            kProducer.produce(
                                    AppConfigs.topicName1, number, "Simple Message-T1-" + number);
                            kProducer.produce(
                                    AppConfigs.topicName2, number, "Simple Message-T1-" + number);
                        }));
        logger.info("Committing First Transaction.");
        kProducer.commitTransaction();
        logger.info("Starting Second Transaction...");

        //TESTING AN ABORTED TRANSACTION

        kProducer.beginTransaction();
        Stream.of(1,2)
                .forEach(checkedExceptionHandler(number -> {
                            kProducer.produce(AppConfigs.topicName1, number, "Simple Message-T2-" + number);
                            kProducer.produce(AppConfigs.topicName2, number, "Simple Message-T2-" + number);
                        }));
        logger.info("Aborting Second Transaction.");
        kProducer.abortTransaction();
        logger.info("Finished - Closing Kafka Producer.");
        kProducer.close();

    }

    static <GenType> Consumer<GenType> checkedExceptionHandler(Consumer<GenType> consumer){
        return obj -> {
            try{
                consumer.accept(obj);
            }catch (Exception ex){
                logger.error("Exception in Transaction. Aborting...");
                kProducer.abortTransaction();
                kProducer.close();
                throw new RuntimeException(ex);
            }
        };
    }

}
