package guru.learningjournal.kafka.examples;

class AppConfigs {
    final static String applicationID = "HelloProducer";


    final static int numEvents = 2;
    final static String transaction_id = "Hello-Producer-Trans";


    final static String bootstrapServers = "localhost:9092, localhost:9093, localhost:9094";
    final static String topicName1 = "atomicity1";
    final static String topicName2 = "atomicity2";

}
