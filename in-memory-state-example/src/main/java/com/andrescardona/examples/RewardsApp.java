package com.andrescardona.examples;

import com.andrescardona.examples.serde.AppSerdes;
import com.andrescardona.examples.transformers.RewardsTransformer;
import com.andrescardona.examples.types.PosInvoice;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class RewardsApp {
    private static final Logger logger = LoggerFactory.getLogger(RewardsApp.class);

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfig.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);

        StreamsBuilder sbuilder = new StreamsBuilder();
        KStream<String, PosInvoice> KS0 = sbuilder.stream(
                AppConfig.posTopicName,
                Consumed.with(AppSerdes.String(), AppSerdes.PosInvoice()))
                .filter((key,value) ->
                value.getCustomerType().equalsIgnoreCase(AppConfig.CUSTOMER_TYPE_PRIME));

        StoreBuilder kvStoreBuilder = Stores.keyValueStoreBuilder(
                Stores.inMemoryKeyValueStore(AppConfig.REWARDS_STORE_NAME),
                AppSerdes.String(), AppSerdes.Double());

        sbuilder.addStateStore(kvStoreBuilder);

        KS0
//                .repartition()//Alternative to the deprecated through("String topic, ProducedWith(Serdes key, Serdes value)) Kafka takes care of the topic for you
                .transformValues(() -> new RewardsTransformer(), AppConfig.REWARDS_STORE_NAME)
                .to(AppConfig.notificationTopic,
                        Produced.with(AppSerdes.String(), AppSerdes.Notification()));

        KafkaStreams stream = new KafkaStreams(sbuilder.build(), props);
        stream.
                start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Closing app gracefully");
            stream.close();
        }));

    }
}
