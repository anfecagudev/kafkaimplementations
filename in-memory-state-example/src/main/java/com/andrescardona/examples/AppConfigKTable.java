package com.andrescardona.examples;

public class AppConfigKTable {
    public final static String applicationID = "StreamingTable";
    public final static String bootstrapServers = "localhost:9092,localhost:9093, localhost:9094";
    public final static String posTopicName = "stock-tick";
    final static String stateStoreLocation = "tmp/state-store";
    final static String stateStoreName = "kt01-store";
    final static String regExSymbol = "(?i)HDFCBANK|TCS";
    final static String queryServerHost = "localhost";
    final static int queryServerPort = 7010;
}
