package com.andrescardona.examples.partitioners;

import com.andrescardona.examples.types.PosInvoice;
import org.apache.kafka.streams.processor.StreamPartitioner;

public class RewardPartitioner implements StreamPartitioner<String, PosInvoice> {
    @Override
    public Integer partition(String topic, String key, PosInvoice value, int numPartitions) {
        return value.getCustomerCardNo().hashCode() % numPartitions;
    }
}
