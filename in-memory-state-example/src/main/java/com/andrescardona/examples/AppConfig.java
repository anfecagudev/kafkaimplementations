package com.andrescardona.examples;

public class AppConfig {
    public final static String applicationID = "RewardsApp";
    public final static String bootstrapServers = "localhost:9092,localhost:9093, localhost:9094";
    public final static String posTopicName = "invoices";
    public final static String notificationTopic = "loyalty";
    public final static String CUSTOMER_TYPE_PRIME = "PRIME";
    public final static Double LOYALTY_FACTOR = 0.02;
    public final static String REWARDS_STORE_NAME = "CustomerRewardsStore";
}
