package com.andrescardona.configuration;

public class KafkaStreamsConfig {
    public static final String bootstrapServers = "localhost:9092, localhost:9093, localhost:9094";
    public static final String AppId = "StudentStreamer";
    public static final String topic = "students";

    public static final String stateStoreLocation = "tmp/state-store2";
}
